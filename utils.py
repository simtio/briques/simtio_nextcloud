import socket
import logging
import time


def make_php_conf(d):
    s = ['\"{}\"\t=> \"{}\",\n'.format(k, v) for k, v in d.items()]
    return '<?php CONFIG = array ({});'.format(s)


def get_subdict(d, sub_key):
    return {key: value for (key, value) in d.items() if key.startswith(sub_key)}


def wait_socket(ip, port, tries=300):
    retry_count = 0
    while retry_count < tries:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((ip, port))
        if result == 0:
            logging.debug("Nextcloud stack deployed")
            break
        logging.debug("Nextcloud not ready, i will retry...")
        retry_count += 1
        sock.close()
        time.sleep(2)
