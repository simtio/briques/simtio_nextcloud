from core import BasePlugin
from core import MenuItem
from core import Page
from core import Config
from core import Wizard
from core import DockerManager
from bottle import request
import re
from core import UserDashboardItem
from core import UserDashboardManager
from core import PluginManager
from plugins.nextcloud.utils import get_subdict, wait_socket
from plugins.nextcloud.nextcloud import NextcloudAPI


class Plugin(BasePlugin):

    def __init__(self):
        super(Plugin, self).__init__()
        self.register_menu_item(MenuItem("Nextcloud", 'fas fa-box-open', "/plugins/nextcloud/"))

        self.register_page(Page('', Page.METHOD_GET, self.action_index))
        self.register_page(Page('', Page.METHOD_POST, self.action_index_process))

        self.dns_plugin = lambda: PluginManager().has_plugin('dns')
        self.ldap_plugin = lambda: PluginManager().has_plugin('ldap')
        self.smtp_plugin = lambda: PluginManager().has_plugin('smtp_relay')

        self.domain = ''
        if self.dns_plugin:
            self.domain = Config().get('plugins')['dns']['dns_domain']

        self.api = NextcloudAPI(Config().get('worker_ip'), self)
        self.config.set("config_file", Config().get('data_location') + "/{}".format(self.api.mail_filename))

        UserDashboardManager().append_item(
            UserDashboardItem("Nextcloud", "/plugins/nextcloud/assets/images/logo.png",
                              'http://nextcloud.{}'.format(self.domain), self.manage_roles))

        self.register_wizard(Wizard('wiz_apps', self.strings.wiz_apps_prompt, response_type=Wizard.TYPE_BOOL))
        self.register_wizard(Wizard('wiz_pwd', self.strings.wiz_pwd_prompt, response_type=Wizard.TYPE_PASSWORD))

        # add thruster domain
        DockerManager().exec_cmd('plugins_nextcloud_nextcloud',
                                 '-u nextcloud /config/www/nextcloud/occ config:system:set trusted_domains 2 --value=nextcloud.'+self.domain)

        # parse ldap conf template
        if self.ldap_plugin:
            self.api.parse_conf_template('{{DOMAIN}}', Config().get('plugins')['ldap']['base_dn'])
        self.api.parse_conf_template('{{HOST}}', Config().get('worker_ip'))

    def manage_roles(self):
        return self.config.get('ldap_groups')

    def action_index(self, msg_domain=None, msg_add=None):
        return self.render("index", {"name": "Nextcloud",
                                     "admin_conf": {'admin_pwd': self.config.get('wiz_pwd')},
                                     "ldap_conf": get_subdict(self.config.get(), 'ldap'),
                                     "smtp_conf": get_subdict(self.config.get(), 'mail'),
                                     # "available_groups": self.get_groups(),
                                     # "nextcloud_groups": self.config.get('ldap_groups')
                                     })

    def action_index_process(self):
        request.body.read()
        f = request.forms

        # parse admin
        if self.parse_form(f, "admin_password"):
            self.api.reset_admin_pwd(f["admin_password"])

        # parse smtp
        if self.parse_form(f, "smtp_host", r"^(\d{0,3}.){3}\d{0,3}$") or \
           self.parse_form(f, "smtp_port", r"\d*") or \
           self.parse_form(f, "smtp_auth") or \
           self.parse_form(f, "smtp_login", r"\w*") or \
           self.parse_form(f, "smtp_password"):
            self.update_smtp_conf(Config().get('config_file'))

        # parse ldap
        self.update_ldap_conf('s01')
        # if self.parse_form(f, "ldapHost", r"^(\d{0,3}.){3}\d{0,3}$") or \
        #   self.parse_form(f, "ldapPort", r"\d*") or \
        #   self.parse_form(f, "ldapBase") or \
        #   self.parse_form(f, "ldapBaseUsers") or \
        #   self.parse_form(f, "ldapBaseGroups"):
        #   self.update_ldap_conf('s01')

        return self.action_index()

    def parse_form(self, form, name, regex=r'.*'):
        keys = form.keys()
        if name in keys:
            if re.match(regex, form[name]):
                actual = self.config.get(name)
                new = form[name]
                if actual == new:
                    return False
                else:
                    self.config.set(name, new)
                    return True

    def install(self):
        # set default ldap conf
        self.config.update(self.api.default_ldap_conf)

        # set default smtp conf
        self.config.update(self.api.default_mail_conf)

        # deploy stack
        super().install()
        wait_socket(Config().get('worker_ip'), 8090)

        # add DNS entry
        if self.dns_plugin():
            PluginManager().get_plugin('plugins.dns').add_entry('nextcloud', "simtio." + self.domain, 'CNAME')

        # create admin user
        self.api.init_admin(self.config.get('wiz_apps'))

        # enable & config ldap
        if self.ldap_plugin():
            grps = [g['cn'].value for g in PluginManager().get_plugin('plugins.ldap').get_groups()]
            if 'nextcloud' not in grps:
                PluginManager().get_plugin('plugins.ldap').ldap.add_group('nextcloud')

            self.api.enable_app('user_ldap')
            self.api.create_ldap_conf()

            # conf = {
            #     'ldapHost': Config().get('worker_ip'),
            #     'ldapBase': Config().get('plugins')['ldap']['base_dn']
            #     }
            # self.config.update(conf)
            self.update_ldap_conf()

        # config smtp
        if self.smtp_plugin():
            conf = {'mail_smtphost': Config().get('worker_ip')}
            self.config.update(conf)
            self.update_smtp_conf()

    def update_ldap_conf(self, id='s01'):
        self.clean()
        ldap_conf = get_subdict(self.config.get(), 'ldap')
        self.api.update_ldap_conf(ldap_conf, id)

    def update_smtp_conf(self):
        smtp_conf = get_subdict(self.config.get(), 'mail')
        self.api.update_smtp_conf(self.config.get('config_file'), smtp_conf)

    def clean(self):
        for k, v in get_subdict(self.config.get(), 'ldap').items():
            if k not in self.api.default_ldap_conf.keys():
                del(self.config.data['nextcloud'][k])
