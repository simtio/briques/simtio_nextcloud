import requests
import re
import subprocess
from plugins.nextcloud.utils import make_php_conf


class NextcloudAPI:

    default_mail_conf = {
        "mail_smtpmode": "smtp",
        "mail_smtphost": "0.0.0.0",
        "mail_smtpport": 25,
        "mail_smtptimeout": 10,
        "mail_smtpsecure": "",
        "mail_smtpauth": 'false',
        "mail_smtpauthtype": "LOGIN",
        "mail_smtpname": "",
        "mail_smtppassword": ""}

    mail_filename = 'mail.config.php'

    default_ldap_conf = {
        "ldapHost": "{{HOST}}",
        "ldapPort": "389",
        # "ldapBackupHost": "",
        # "ldapBackupPort": "",
        "ldapBase": "{{DOMAIN}}",
        "ldapBaseUsers": "{{DOMAIN}}",
        "ldapBaseGroups": "{{DOMAIN}}",
        # "ldapAgentName": "",
        # "ldapAgentPassword": "***",
        # "ldapTLS": "0",
        # "turnOffCertCheck": "0",
        # "ldapIgnoreNamingRules": "",
        "ldapUserDisplayName": "displayName",
        # "ldapUserDisplayName2": "",
        # "ldapUserAvatarRule": "default",
        # "ldapGidNumber": "gidNumber",
        "ldapUserFilterObjectclass": "inetOrgPerson",
        "ldapUserFilterGroups": "nextcloud",
        "ldapUserFilter": "(&amp;(|(objectclass=inetOrgPerson))(|(memberof=cn=nextcloud,ou=groups,{{DOMAIN}})))",
        # "ldapUserFilterMode": "0",
        "ldapGroupFilter": "(&amp;(|(objectclass=groupOfNames))(|(cn=nextcloud)))",
        # "ldapGroupFilterMode": "0",
        "ldapGroupFilterObjectclass": "groupOfNames",
        "ldapGroupFilterGroups": "nextcloud",
        "ldapGroupDisplayName": "cn",
        "ldapGroupMemberAssocAttr": "member",
        "ldapLoginFilter": "(&amp;(&amp;(|(objectclass=inetOrgPerson))(|(memberof=cn=nextcloud,ou=groups,{{DOMAIN}})))(uid=%uid))",
        # "ldapLoginFilterMode": "0",
        # "ldapLoginFilterEmail": "0",
        # "ldapLoginFilterUsername": "1",
        # "ldapLoginFilterAttributes": "",
        # "ldapQuotaAttribute": "",
        # "ldapQuotaDefault": "",
        # "ldapEmailAttribute": "",
        # "ldapCacheTTL": "600",
        # "ldapUuidUserAttribute": "auto",
        # "ldapUuidGroupAttribute": "auto",
        # "ldapOverrideMainServer": "",
        # "ldapConfigurationActive": "1",
        # "ldapAttributesForUserSearch": "",
        # "ldapAttributesForGroupSearch": "",
        # "ldapExperiencedAdmin": "0",
        # "homeFolderNamingRule": "",
        # "hasMemberOfFilterSupport": "1",
        # "useMemberOfToDetectMembership": "1",
        # "ldapExpertUsernameAttr": "",
        # "ldapExpertUUIDUserAttr": "",
        # "ldapExpertUUIDGroupAttr": "",
        # "lastJpegPhotoLookup": "0",
        # "ldapNestedGroups": "0",
        # "ldapPagingSize": "500",
        # "turnOnPasswordChange": "0",
        # "ldapDynamicGroupMemberURL": "",
        # "ldapDefaultPPolicyDN": "",
        # "ldapExtStorageHomeAttribute": ""
    }

    def __init__(self, ip, parent):
        self.worker = ip
        self.parent = parent

    def init_admin(self, apps):
        login = 'admin'
        pwd = self.parent.config.get('wiz_pwd')

        payload = {'install': 'true',  'adminlogin': login, 'adminpass': pwd, 'adminpass-clone': pwd,
                   'directory': '/data', 'dbtype': 'sqlite', 'dbhost': 'localhost',
                   'dbuser': '', 'dbpass': '', 'dbpass-clone': '', 'dbname': ''}

        if self.parent.config.get('wiz_apps'):
            payload['install-recommended-apps'] = 'on'

        self.fire('post', '/index.php', headers={'content-type': 'application/x-www-form-urlencoded'}, data=payload, anon=True)

    def list_users(self):
        self.fire('get', '/ocs/v1.php/cloud/users', {'content-type': 'application/json', 'Accept-Charset': 'UTF-8', })

    def create_ldap_conf(self):
        self.fire('post', '/ocs/v2.php/apps/user_ldap/api/v1/config',
                  {'content-type': 'application/x-www-form-urlencoded', 'Accept-Charset': 'UTF-8', })

    def read_ldap_conf(self, id):
        self.fire('get', '/ocs/v2.php/apps/user_ldap/api/v1/config/{}'.format(id), {'content-type': 'application/json', 'Accept-Charset': 'UTF-8', })

    def enable_app(self, app_id):
        # user_ldap
        self.fire('post', '/ocs/v1.php/cloud/apps/{}'.format(app_id), {'content-type': 'application/json', 'Accept-Charset': 'UTF-8', })

    def fire(self, x, url, headers={}, data=None, params=None, anon=False):
        base_headers = {'OCS-APIRequest': 'true'}

        auth = '' if anon else 'admin:{}@'.format(self.parent.config.get('wiz_pwd'))

        base_url = 'http://{}{}:8090'.format(auth, self.worker)

        base_headers.update(headers)

        with requests.Session() as session:
            r = getattr(session, x)(base_url+url, headers=base_headers, data=data, params=params)
            print(r, r.text)

    def update_ldap_conf(self, conf, id):
        final = subdict = {}
        for k, v in conf.items():
            if k == 'ldap_groups' and len(v) > 0:
                f = ['(cn={})'.format(item) for item in v]
                v = '(&(|(objectclass=groupOfNames))'
                subdict = {'configData["ldapGroupFilter"]': v+'|{'+''.join(f),
                           'configData["ldapGroupFilterGroups"]': ';'.join(v)}
            else:
                subdict = {'configData[{}]'.format(k): v}

            final.update(subdict)

        self.fire('put', '/ocs/v2.php/apps/user_ldap/api/v1/config/{}'.format(id), data=final)

    def update_smtp_conf(self, path, conf):
        with open(path, "w") as f:
            f.write(make_php_conf(conf))

    def parse_conf_template(self, template, value):
        for k, v in self.default_ldap_conf.items():
            self.default_ldap_conf[k] = re.sub(template, value, self.default_ldap_conf[k])

    def reset_admin_pwd(self, pwd):
        cmd = "sh -c 'export OC_PASS=\"{0}\" && php /config/www/nextcloud/occ user:resetpassword --password-from-env admin'".format(pwd)

        out = subprocess.run('docker -H {0} exec -u nextcloud $(docker ps --filter "name=plugins_nextcloud_nextcloud" -q) {1}'
                             .format(self.worker, cmd),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        return out.stderr + out.stdout
