#!/usr/bin/env bash

# Get out if variable is not initialized
set -e

# Set path
PATH=/bin:/sbin:/usr/bin:/usr/sbin

############
## CONFIG ##
############
# Setup nextcloud user
PUID=${PUID:-911}
PGID=${PGID:-911}
groupmod -o -g "$PGID" nextcloud
usermod -o -u "$PUID" nextcloud
echo '
--------------------------------------------
  ___   ___   __  __   _____   ___    ___
 / __| |_ _| |  \/  | |_   _| |_ _|  / _ \
 \__ \  | |  | |\/| |   | |    | |  | (_) |
 |___/ |___| |_|  |_|   |_|   |___|  \___/

--------------------------------------------

Brought to you by SIMTIO
https://www.simtio.fr
--------------------------------------------
GID/UID
--------------------------------------------'
echo "
User uid:    $(id -u nextcloud)
User gid:    $(id -g nextcloud)
--------------------------------------------
"

# Setup folders
mkdir -p /config/{nginx/site-confs,www,log/nginx,keys,log/php,php} \
	/run/php /var/lib/nginx/tmp/client_body /var/tmp/nginx /data

# copy config files if it is absent
[[ ! -f /config/nginx/nginx.conf ]] && \
	cp /defaults/nginx.conf /config/nginx/nginx.conf
[[ ! -f /config/nginx/site-confs/default ]] && \
	cp /defaults/default /config/nginx/site-confs/default
[[ $(find /config/www -type f | wc -l) -eq 0 ]] && \
	cp /defaults/index.html /config/www/index.html

# fix php-fpm log location
sed -i "s#error_log = /var/log/php7.3-fpm.log.*#error_log = /config/log/php/error.log#g" /etc/php/7.3/fpm/php-fpm.conf
# fix php-fpm user
sed -i "s#user = www-data#user = nextcloud#g" /etc/php/7.3/fpm/pool.d/www.conf
sed -i "s#group = www-data#group = nextcloud#g" /etc/php/7.3/fpm/pool.d/www.conf

############
## KEYGEN ##
############
SUBJECT="/C=FR/ST=Rhones-Alpes/L=Lyon/O=simtio.fr/OU=Simtio Server/CN=*"
if [[ -f /config/keys/cert.key && -f /config/keys/cert.crt ]]; then
echo "using keys found in /config/keys"
else
echo "generating self-signed keys in /config/keys, you can replace these with your own keys if required"
openssl req -new -x509 -days 3650 -nodes -out /config/keys/cert.crt -keyout /config/keys/cert.key -subj "$SUBJECT"
fi

#################
## PERMISSIONS ##
#################
chown nextcloud:nextcloud /defaults /data
chown -R nextcloud:nextcloud /config /var/lib/nginx /var/tmp/nginx
chmod -R g+w /config/{nginx,www}
chmod -R 644 /etc/logrotate.d
chmod +x "${NEXTCLOUD_PATH}/occ"

###########
## CRONS ##
###########
crontab /defaults/nextcloud

#####################
## Manage services ##
#####################
service php7.3-fpm restart
service nginx restart

# Set trap to stop the script proprely when a docker stop is executed
trap : EXIT TERM KILL INT SIGKILL SIGTERM SIGQUIT

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container will exit with an error
# if it detects that either of the processes has exited.
# Otherwise it will loop forever, waking up every 60 seconds
while /bin/true; do
  ps aux |grep nginx |grep -q -v grep
  NGINX_STATUS=$?
  ps aux |grep php-fpm |grep -q -v grep
  FPM_STATUS=$?
  # If the greps above find anything, they will exit with 0 status
  # If they are not both 0, then something is wrong
  if [ ${NGINX_STATUS} -ne 0 -o ${FPM_STATUS} -ne 0 ]; then
    echo "One of the processes has already exited."
    exit -1
  fi
  echo "Healthcheck OK"
  sleep 120
done
