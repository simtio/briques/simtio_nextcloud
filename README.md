# SIMTIO - Nextcloud

This container contain Nginx, PHP-7.3 and Nextcloud.

## Starting the container & Configuring


```
docker run -d -v nextcloud:/config -v nextcloud-data:/data -p 80:80 --name=simtio-nextcloud registry.gitlab.com/simtio/briques/simtio_nextcloud
```

## Dev
```
# Gitlab
docker build -t registry.gitlab.com/simtio/briques/simtio_nextcloud:latest -t registry.gitlab.com/simtio/briques/simtio_nextcloud:0.x .
docker push registry.gitlab.com/simtio/briques/simtio_nextcloud && docker push registry.gitlab.com/simtio/briques/simtio_nextcloud:0.x

# Dockerhub
docker build -t simtio/nextcloud:latest -t simtio/nextcloud:0.x .
docker push simtio/nextcloud:latest && docker push simtio/nextcloud:0.x

# MultiArch
docker buildx build -t simtio/nextcloud:latest -t simtio/nextcloud:0.x --platform=linux/aarch64,linux/amd64,linux/arm . --push

# Run
docker run --rm -p 80:80 --name=simtio-nextcloud simtio/nextcloud
```

## Sources
- https://docs.nextcloud.com/server/16/admin_manual/installation/source_installation.html#example-installation-on-ubuntu-18-04-lts-server
- https://howto.wared.fr/ubuntu-installation-nextcloud-nginx/
- https://github.com/linuxserver/docker-baseimage-alpine-nginx
- https://github.com/linuxserver/docker-nextcloud
- https://github.com/nextcloud/docker/
- https://stackoverflow.com/questions/19948149/can-i-run-multiple-programs-in-a-docker-container
- https://tecadmin.net/install-php-debian-9-stretch/
