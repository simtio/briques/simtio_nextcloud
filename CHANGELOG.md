# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.7] - 2020-04-26
### Fixed
- update locales

## [1.1.6] - 2020-04-19
### Fixed
- misc fix
- clean conf
  
## [1.1.5] - 2020-04-17
### Fixed
- en support 
  
## [1.1.4] - 2020-04-17
### Fixed
- config 
  
## [1.1.3] - 2020-04-16
### Added
- added apps options
  
## [1.1.2] - 2020-04-16
### Changed
- hotfix brick admin init
  
## [1.1.1] - 2020-04-16
### Changed
- wizard
- admin login policy

### Removed
- smtp options (temporary removal)
  
## [1.1.0] - 2020-04-15
### Added
- mulitlang description
  
## [1.0.19] - 2020-04-14
### Added
- fix domain
  
## [1.0.18] - 2020-04-14
### Added
- ldap imporove template 
- added admin reset pwd 
  
## [1.0.17] - 2020-04-14
### Added
- ldap config template
- data volume 

## [1.0.16] - 2020-03-17
### Changed
- fix dns
- added thrusted domain
  
## [1.0.15] - 2020-03-17
### Changed
- DNS entry
- Dahsboard link
- Update assets
   
## [1.0.14] - 2020-03-01
### Changed
- Changed base_image (Stretch to Buster)
- Fixed gitlab-ci to build multiarch

## [1.0.13] - 2020-02-11
### Changed
- fix grp
  
## [1.0.12] - 2020-02-11
### Changed
- fix ui
- add dns entry
  
## [1.0.11] - 2020-02-11
### Changed
- fix install (waiting socket)
- fix ldap conf request
  
### Removed
- useless new ldap conf
  
## [1.0.10] - 2020-02-08
### Changed
- ldap conf
  
## [1.0.9] - 2020-02-08
### Changed
- mail conf
  
## [1.0.8] - 2020-02-02
### Changed
- fix misc, update ldap conf, update nextcloud groups strategy
  
## [1.0.7] - 2020-02-02
### Changed
- fix misc
  
## [1.0.6] - 2020-02-02
### Changed
- fix admin queries
  
## [1.0.5] - 2020-02-02
### Changed
- Fix Nextcloud query launcher

## [1.0.4] - 2020-02-01
### Changed
- Fix Dockerfile

## [1.0.3] - 2020-01-28
### Added
- Wizard support
  
## [1.0.2] - 2019-12-25
### Changed
- Fixed auto-redirect issue to SSL in php-fpm configuration

## [1.0.1] - 2019-12-20
### Changed
- Brick name

## [1.0.0] - 2019-12-20
### Added
- Nextcloud brick integration
